import actions.Action;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class GameState implements Cloneable {
    private final int ROLL = 0, CLAIM = 1, RESPONSE = 2;

    protected int numSides;
    protected int numDice;
    public int numClaims;
    boolean[] claimHistory;
    Action[] previousActions;
    Action latestAction;

    public GameState(int numSides, int numDice) {
        this.numDice = numDice;
        this.numSides = numSides;
        numClaims = numDice * numSides;

        previousActions = new Action[3];
        claimHistory = new boolean[numClaims];

        latestAction = new Action(0, 1, Action.ROLL_ACTION);
        previousActions[ROLL] = latestAction;
        previousActions[CLAIM] = new Action(-1, 0, Action.CLAIM_ACTION);
    }

    public List<Action> generateActions(int nextPlayer) {
        List<Action> actions = new LinkedList<>();

        if (latestAction.type == Action.ROLL_ACTION) {
            for (int i = previousActions[CLAIM].actionNumber + 1; i < numClaims; i++) {
                actions.add(new Action(i, nextPlayer, Action.CLAIM_ACTION));
            }
        } else if (latestAction.type == Action.CLAIM_ACTION){
            actions.add(new Action(Action.ACCEPT, nextPlayer, Action.RESPONSE_ACTION));
            actions.add(new Action(Action.DENY, nextPlayer, Action.RESPONSE_ACTION));
        } else if (latestAction.type == Action.RESPONSE_ACTION &&
                latestAction.actionNumber != Action.DENY){
            for (int i = 0; i < numClaims; i++) {
                actions.add(new Action(i, nextPlayer, Action.ROLL_ACTION));
            }
        }

        return actions;
    }

    public int nextPlayer() {
        if (latestAction == null) {
            return 0;
        }

        if (latestAction.type == Action.CLAIM_ACTION) {
            return Math.abs(latestAction.player - 1);
        } else {
            return latestAction.player;
        }
    }

    public void makeMove(Action action) {
        latestAction = action;

        if (action.type == Action.ROLL_ACTION) {
            previousActions[ROLL] = action;

        } else if (action.type == Action.CLAIM_ACTION) {
            previousActions[CLAIM] = action;
            claimHistory[action.actionNumber] = true;

        } else {
            previousActions[RESPONSE] = action;
        }
    }

    public boolean isTerminal() {
        return latestAction.type == Action.RESPONSE_ACTION && latestAction.actionNumber == Action.DENY;
    }

    public boolean isChanceNode() {
        return latestAction.type == Action.RESPONSE_ACTION;
    }

    public int getScore() {
        if (previousActions[ROLL].actionNumber >= previousActions[CLAIM].actionNumber) {
            if (latestAction.player == 0) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (latestAction.player == 0) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    @Override
    public GameState clone() {
        GameState gs = new GameState(numSides, numDice);

        gs.claimHistory = this.claimHistory.clone();
        gs.previousActions = this.previousActions.clone();
        gs.latestAction = this.latestAction.clone();

        return gs;
    }

    @Override
    public int hashCode() {
        if (latestAction.type == Action.ROLL_ACTION) {
            return Objects.hash(claimHistory, latestAction.type, 0);
        } else {
            return Objects.hash(claimHistory, latestAction.type, latestAction.actionNumber);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("{history:");

        for (int i = 0; i < numClaims; i++) {
            if (claimHistory[i]) {
                if (stringBuilder.length() > 0) {
                    stringBuilder.append(',');
                }

                stringBuilder.append(Action.claimNumToString(i));
            }
        }

        stringBuilder.append("}\tLatestAction:{" + latestAction.toString());

        stringBuilder.append("}");


        return stringBuilder.toString();
    }
}
