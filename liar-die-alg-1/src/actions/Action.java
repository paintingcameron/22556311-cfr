package actions;

import java.util.Objects;

public class Action implements Cloneable {
    public final static int ROLL_ACTION = 0, CLAIM_ACTION = 1, RESPONSE_ACTION = 2;
    public final static int ACCEPT = 0, DENY = 1;
    public static int numSides;
    public static int numDice;

    public int actionNumber;
    public int player;
    public int type;

    public Action(int actionNumber, int player, int type) {
        this.actionNumber = actionNumber;
        this.player = player;
        this.type = type;
    }

    @Override
    public Action clone() {
        return new Action(actionNumber, player, type);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Action action = (Action) o;

        return actionNumber == action.actionNumber && player == action.player && type == action.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(actionNumber, player, type);
    }

    public static String claimNumToString(int num) {
        return (Math.floorDiv(num, numSides) + 1) + "x" + (num % numSides + 1);
    }

    public String toString() {
        if (this.type == Action.ROLL_ACTION) {
            return "Roll: " + (actionNumber + 1);
        } else if (this.type == Action.CLAIM_ACTION) {
            return "Claim: " + claimNumToString(this.actionNumber);
        } else {
            return "Response: " + ((this.actionNumber == Action.ACCEPT) ? "Accept" : "Deny");
        }
    }
}

