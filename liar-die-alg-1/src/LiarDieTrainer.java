import actions.Action;

import java.sql.SQLOutput;
import java.util.*;

public class LiarDieTrainer {
    Random rand = new Random();

    Map<GameState, Map<Action, Double>> regretTables;
    Map<GameState, Map<Action, Double>> strategyTables;
    Map<GameState, Map<Action, Double>> strategyProfiles;

    public LiarDieTrainer(int numSides, int numDice) {
        regretTables = new HashMap<GameState, Map<Action, Double>>();
        strategyTables = new HashMap<GameState, Map<Action, Double>>();
        strategyProfiles = new HashMap<GameState, Map<Action, Double>>();

        Action.numSides = numSides;
        Action.numDice = numDice;
    }

    public void initTable(GameState gs) {

        if (regretTables.containsKey(gs)) {
            return;
        }

        HashMap<Action, Double> regrets = new HashMap<Action, Double>();
        HashMap<Action, Double> sumStrategy = new HashMap<Action, Double>();
        HashMap<Action, Double> strategyDist = new HashMap<Action, Double>();

        List<Action> actions = gs.generateActions(gs.nextPlayer());

        for (Action action: actions) {
            regrets.put(action, 0.0);
            sumStrategy.put(action, 0.0);
            strategyDist.put(action, 1.0 / actions.size());

            regretTables.put(gs, regrets);
            strategyTables.put(gs, sumStrategy);
            strategyProfiles.put(gs, strategyDist);

            GameState gsClone = gs.clone();
            gsClone.makeMove(action);

            initTable(gsClone);
        }
    }

    public double cfr(GameState gs, int player, int t, double p1, double p2) {
        if (gs.isTerminal()) {
            return gs.getScore();
        } else if (gs.isChanceNode()) {
            List<Action> actions = gs.generateActions(gs.nextPlayer());
            Action action = actions.get(rand.nextInt(actions.size()));

            GameState ha = gs.clone();
            ha.makeMove(action);

            return cfr(gs, player, t, p1, p2);
        }

        // Should determinize gs. For example, no matter what the previous roll was,
        // we only need to know there was a roll

        double vSig = 0;        // Utility

        Map<Action, Double> vSigToA = new HashMap<Action, Double>();
        List<Action> actions = gs.generateActions(gs.nextPlayer());

        for (Action action: actions) {
            vSigToA.put(action, 0.0);
        }

        Map<Action, Double> sigI = strategyProfiles.get(gs);

        for (Action action: actions) {
            GameState ha = gs.clone();
            ha.makeMove(action);

            if (gs.nextPlayer() == 0) {
                vSigToA.put(action, cfr(ha, player, t, sigI.get(action) * p1, p2));
            } else {
                vSigToA.put(action, cfr(ha, player, t, p1, sigI.get(action) * p2));
            }

            vSig += sigI.get(action) * vSigToA.get(action);
        }

        if (gs.nextPlayer() == player) {
            double sumCumRegrets = 0;

            for (Action action: actions) {
                regretTables.get(gs).put(action, regretTables.get(gs).get(action) +
                        ((player == 0) ? p2 : p1) * (vSigToA.get(action) - vSig));

                strategyTables.get(gs).put(action, strategyTables.get(gs).get(action) +
                        ((player == 0) ? p1 : p2) * sigI.get(action));

                sumCumRegrets += Math.max(regretTables.get(gs).get(action), 0);
            }

            double uniformProb = 1.0 / actions.size();

            for (Action action: actions) {
                if (sumCumRegrets > 0) {
                    strategyProfiles.get(gs).put(action,
                            Math.max(regretTables.get(gs).get(action), 0) / sumCumRegrets);
                } else {
                    strategyProfiles.get(gs).put(action, uniformProb);
                }
            }
        }

        return vSig;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (GameState key: regretTables.keySet()) {
            stringBuilder.append(key + "\n");
            stringBuilder.append("Regrets:\n");
            for (Action action: regretTables.get(key).keySet()) {
                stringBuilder.append(action + ":" + regretTables.get(key).get(action) + ", ");
            }

            stringBuilder.append("\nStrategy Tables:\n");
            for (Action action: strategyTables.get(key).keySet()) {
                stringBuilder.append(action + ":" + strategyTables.get(key).get(action) + ", ");
            }

            stringBuilder.append("\nStrategy Profiles:\n");
            for (Action action: strategyProfiles.get(key).keySet()) {
                stringBuilder.append(action + ":" + strategyProfiles.get(key).get(action) + ", ");
            }

            stringBuilder.append("\n\n");
        }

        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        int numSides = Integer.parseInt(args[0]);
        int numDice = Integer.parseInt(args[1]);
        int iterations = Integer.parseInt(args[2]);

        LiarDieTrainer trainer = new LiarDieTrainer(numSides, numDice);

        GameState gs = new GameState(numSides, numDice);
        trainer.initTable(gs);

        System.out.println(trainer.toString());
    }
}
