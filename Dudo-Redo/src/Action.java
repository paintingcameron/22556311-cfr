import java.io.Serializable;
import java.util.Objects;

public class Action implements Serializable, Cloneable, Comparable<Action> {
    int claim;
    int player;
    int numSides;

    public Action(int claim, int player, int numSides) {
        this.claim = claim;
        this.player = player;
        this.numSides = numSides;
    }

    public int getActionNumber() {
        return claim;
    }

    public int getClaimNum() {
        return Math.floorDiv(claim, numSides) + 1;
    }

    public int getClaimRank() {
        return claim % numSides + 1;
    }

    @Override
    protected Action clone() {
        return new Action(this.claim, this.player, this.numSides);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Action action = (Action) o;
        return claim == action.claim && player == action.player;
    }

    @Override
    public int hashCode() {
        return Objects.hash(claim, player);
    }

    @Override
    public int compareTo(Action o) {
        int num = getClaimNum();
        int rank = getClaimRank();

        int oNum = o.getClaimNum();
        int oRank = o.getClaimRank();

        if (num == oNum && rank == oRank) {
            return 0;
        }

        if (num == oNum) {
            if (rank == 1) {
                return 1;
            } else if (oRank == 1) {
                return -1;
            } else {
                return rank > oRank ? 1 : -1;
            }
        } else {
            return num > oNum ? 1 : -1;
        }
    }

    @Override
    public String toString() {
        if (claim == numSides * 2) {
            return "Dudo";
        }

        return getClaimNum() + "*" + getClaimRank();
    }
}