import java.util.LinkedList;
import java.util.List;

public class GameState implements Cloneable {
    int numSides, numActions, dudo;
    int[] claimRank; // Number of faces
    int[] claimNum; // Dice face

    boolean[] isClaim;
    int player1Roll, player2Roll;
    Action prevClaim, latestClaim;

    public GameState(int sides, int player1Roll, int player2Roll) {
        this.player1Roll = player1Roll;
        this.player2Roll = player2Roll;

        numSides = sides;

        numActions = numSides * 2 + 1;
        dudo = numActions - 1;

        claimRank = new int[numActions];
        claimNum = new int[numActions];

        for (int i = 0; i < numActions-1; i++) {
            claimRank[i] = i % numSides + 1;
            claimNum[i] = Math.floorDiv(i, numSides) + 1;
        }

        isClaim = new boolean[numActions];

        prevClaim = new Action(-1, 0, numSides);
        latestClaim = new Action(-1, 1, numSides);
    }

    public GameState(int sides, boolean[] isClaim, int player1Roll, int player2Roll) {
        this.isClaim = isClaim;

        this.player1Roll = player1Roll;
        this.player2Roll = player2Roll;

        numSides = sides;
        numActions = numSides * 2 + 1;
        dudo = numActions - 1;

        claimRank = new int[numActions];
        claimNum = new int[numActions];

        for (int i = 0; i < numActions-1; i++) {
            claimRank[i] = i % numSides + 1;
            claimNum[i] = Math.floorDiv(i, numSides) + 1;
        }
    }

    public void makeMove(Action action) {
        prevClaim = latestClaim;
        latestClaim = action;
        isClaim[action.getActionNumber()] = true;
    }

    public boolean isTerminal() {
        return isClaim[dudo];
    }

    public boolean isChanceNode() {
        return false;
    }

    public int getScore() {
        int playeri = latestClaim.player;

        boolean falseDudo;

        //TODO: fix scoring!!

        if (prevClaim.getClaimNum() == 2) {
            falseDudo = player1Roll == player2Roll && player1Roll + 1 == prevClaim.getClaimRank();
        } else {
            falseDudo = player1Roll + 1 == prevClaim.getClaimRank() ||
                    player2Roll + 1 == prevClaim.getClaimRank();
        }

        if (falseDudo) {
            if (playeri == 0) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (playeri == 0) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public boolean isValid(Action action) {
        if (latestClaim.getActionNumber() == -1) {
            return true;
        }

        if (action.getActionNumber() == dudo && latestClaim.getActionNumber() != dudo) {
            return true;
        }

        return action.compareTo(latestClaim) > 0;
    }

    public List<Action> getActions(int player) {
        List<Action> actions = new LinkedList<Action>();

        for (int i = 0; i < isClaim.length; i++) {
            Action action = new Action(i, player, numSides);

            if (isValid(action)) {
                actions.add(action);
            }
        }

        return actions;
    }

    @Override
    public GameState clone() {
        GameState newGS = new GameState(numSides, isClaim.clone(), player1Roll, player2Roll);

        newGS.prevClaim = prevClaim.clone();
        newGS.latestClaim = latestClaim.clone();

        return newGS;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < numActions-1; i++) {
            if (isClaim[i]) {
                if (sb.length() > 0) {
                    sb.append(',');
                }

                sb.append(claimNum[i]);
                sb.append('*');
                sb.append(claimRank[i]);
            }
        }

        if (isClaim[dudo]) {
            sb.append(",Dudo");
        }

        return sb.toString();
    }
}