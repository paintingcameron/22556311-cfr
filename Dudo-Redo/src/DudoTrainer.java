import java.io.Serializable;
import java.util.*;

public class DudoTrainer {

    Map<String, Map<Action, Double>> regretTables;
    Map<String, Map<Action, Double>> strategyTables;
    Map<String, Map<Action, Double>> strategyProfilesT;
    Map<String, Map<Action, Double>> strategyProfilesT1;

    public static void main(String[] args) {
        int numSides = 3;

        int iterations = Integer.parseInt(args[0]);

        DudoTrainer trainer = new DudoTrainer(numSides);

        Random random = new Random();

        int p1 = random.nextInt(numSides * 2);
        int p2 = random.nextInt(numSides * 2);

        GameState gs = new GameState(numSides, p1, p2);
        trainer.initTable(gs);

        double game_util = 0;

//        System.out.println(trainer.toString());
        System.out.println("Starting training");

        for (int i = 0; i < iterations; i++) {
            for (int player = 0; player < 2; player++) {
                double util = trainer.cfr(gs, player, i, 1, 1);

                if (player == 0) {
                    game_util += util;
                }
            }
            // No dependence between iterations and thus has no effect
            trainer.strategyProfilesT = trainer.strategyProfilesT1;
//            trainer.strategyProfilesT1 = cloneStrategyProfile((HashMap<String, Map<Action, Double>>)
//                    trainer.strategyProfilesT1);
        }

        System.out.println(trainer.toString());
        System.out.println("Game utility: " + game_util / iterations);
    }

    public DudoTrainer(int sides) {
        regretTables = new HashMap<String, Map<Action, Double>>();
        strategyTables = new HashMap<String, Map<Action, Double>>();
        strategyProfilesT = new HashMap<String, Map<Action, Double>>();
        strategyProfilesT1 = new HashMap<String, Map<Action, Double>>();
    }

    private static HashMap<String, Map<Action, Double>> cloneStrategyProfile(HashMap<String, Map<Action, Double>> profiles) {
        HashMap<String, Map<Action, Double>> profilesT1 = new HashMap<String, Map<Action, Double>>();

        for (String key: profiles.keySet()) {
            Map<Action, Double> profile = profiles.get(key);
            Map<Action, Double> map = new HashMap<Action, Double>();
            for (Action action: profile.keySet()) {
                map.put(action, profiles.get(key).get(action));
            }
            profilesT1.put(key, map);
        }

        return profilesT1;
    }

    public void initTable(GameState gs) {
        String history = gs.toString();

        if (regretTables.containsKey(history)) {
            return;
        }

        HashMap<Action, Double> regrets = (HashMap<Action, Double>) regretTables.get(history);
        HashMap<Action, Double> sumStrategy = (HashMap<Action, Double>) strategyTables.get(history);
        HashMap<Action, Double> strategyT = (HashMap<Action, Double>) strategyProfilesT.get(history);
        HashMap<Action, Double> strategyT1 = (HashMap<Action, Double>) strategyProfilesT1.get(history);

        regrets = new HashMap<Action, Double>();
        sumStrategy = new HashMap<Action, Double>();
        strategyT = new HashMap<Action, Double>();
        strategyT1 = new HashMap<Action, Double>();

        List<Action> actions = gs.getActions(gs.prevClaim.player);

        for (Action action: actions) {
            regrets.put(action, 0.0);
            sumStrategy.put(action, 0.0);
            strategyT.put(action, 1.0 / actions.size());
            strategyT1.put(action, 1.0 / actions.size());

            regretTables.put(history, regrets);
            strategyTables.put(history, sumStrategy);
            strategyProfilesT.put(history, strategyT);
            strategyProfilesT1.put(history, strategyT1);

            GameState gsClone = gs.clone();
            gsClone.makeMove(action);

            initTable(gsClone);
        }
    }

    public double cfr(GameState gs, int player, int iter, double reach1, double reach2) {
        //Line 6
        if (gs.isTerminal()) {
            //Line 7
            return gs.getScore();
        //Line 8 - 11
        } else if (gs.isChanceNode()) {

        }

        //Line 13
        double utility = 0;

        int nextPlayer = gs.prevClaim.player;

        //Line 14
        Map<Action, Double> vSigIToA = new HashMap<Action, Double>();
        List<Action> actions = gs.getActions(nextPlayer);

        for (Action action: actions) {
            vSigIToA.put(action, 0.0);
        }

        Map<Action, Double> sigI = strategyProfilesT.get(gs.toString());

        for (Action action: actions) {
            GameState ha = gs.clone();
            ha.makeMove(action);

            //Line 16-20
            if (nextPlayer == 0) {
                vSigIToA.put(action, cfr(ha, player, iter, sigI.get(action) * reach1, reach2));
            } else {
                vSigIToA.put(action, cfr(ha, player, iter, reach1, sigI.get(action) * reach2));
            }

            // Line 21
            utility += sigI.get(action) * vSigIToA.get(action);
        }

        String gsStr = gs.toString();

        if (nextPlayer == player) {
            double sumCumulativeRegrets = 0;
            for (Action action: actions) {
                regretTables.get(gsStr).put(action,
                        regretTables.get(gsStr).get(action)
                                + ((player==0)?reach2:reach1) * (vSigIToA.get(action) - utility));

                strategyTables.get(gsStr).put(action, strategyTables.get(gsStr).get(action) +
                        ((player==0)?reach1:reach2) * sigI.get(action));

                sumCumulativeRegrets += Math.max(regretTables.get(gsStr).get(action), 0);
            }

            double uniformProb = 1.0 / actions.size();
            for (Action action: actions) {
                if (sumCumulativeRegrets > 0) {
                    strategyProfilesT1.get(gsStr).put(action, Math.max(regretTables.get(gsStr).get(action), 0) / sumCumulativeRegrets);
                } else {
                    strategyProfilesT1.get(gsStr).put(action, uniformProb);
                }
            }
        }

        return utility;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (String key: regretTables.keySet()) {
            stringBuilder.append(key + "\n");
            stringBuilder.append("Regrets:\n");
            for (Action action: regretTables.get(key).keySet()) {
                stringBuilder.append(action + ":" + regretTables.get(key).get(action) + ", ");
            }

            stringBuilder.append("\nStrategy Tables:\n");
            for (Action action: strategyTables.get(key).keySet()) {
                stringBuilder.append(action + ":" + strategyTables.get(key).get(action) + ", ");
            }

            stringBuilder.append("\nStrategy Profiles:\n");
            for (Action action: strategyProfilesT1.get(key).keySet()) {
                stringBuilder.append(action + ":" + strategyProfilesT.get(key).get(action) + ", ");
            }

            stringBuilder.append("\n\n");
        }

        return stringBuilder.toString();
    }
}

